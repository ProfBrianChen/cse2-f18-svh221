/// Suyeon Hong
/// 10/18/2018
/// CSE 002 - 210
/// lab06
/// To practice using nested loops by creating patterns
import java.util.Scanner;
public class PatternD { // class starts
    public static void main (String arg[]) { // main method
{ 
    Scanner myScanner =  new Scanner ( System.in );

    int i;
    int n;
    int j;
    
    
        System.out.println("Enter an interger between 1 and 10:");
        n = -1;
        while (n < 0 || n > 10) { // checks to see if the range is between 0 and 10 
            while (!myScanner.hasNextInt()){ //if the input is not an integer
                myScanner.next();
                System.out.println("That's not an integer; error entry");
              }
          System.out.println("That's out of range. Enter a value again");
          n = myScanner.nextInt();
         }
        
        for(i = n; i >= 1; i--) // number of rows, decrementing
        { 
            for(j = i; j >= 1; j--) // number of columns, decrementing
            { 
                System.out.print(j+" "); // printing numbers
            } 
            System.out.println(); // ending line after each row 
        } 
    }
    }
}