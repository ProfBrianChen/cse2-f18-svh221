/// Suyeon Hong
/// 10/18/2018
/// CSE 002 - 210
/// lab06
/// To practice using nested loops by creating patterns
import java.util.Scanner;
public class PatternC { // class starts
    public static void main (String arg[]) // main method
    { 
        Scanner myScanner =  new Scanner ( System.in );

        int i;
        int n;
        int j;
        int k;
        
        
        System.out.println("Enter an interger between 1 and 10:");
        
        n = -1;
        while (n < 0 || n > 10) { // checks to see if the range is between 0 and 10 
            while (!myScanner.hasNextInt()){
                myScanner.next(); // if the input is not an integer
                System.out.println("That's not an integer; error entry");
                }
            System.out.println("That's out of range. Enter a value again");
            n = myScanner.nextInt();
            }
        
        for(i = 1; i <= n ; i++) // number of rows
        { 
            for(j = n; j > i; j--) // decrementing spaces
            { 
                System.out.print(" "); // printing spaces
            }
            for (k = i; k > 0; k--) { // decrementing numbers
                System.out.print(k); // printing numbers
            }
            System.out.println(); // moving to the next line after each row
        } 
    }
}