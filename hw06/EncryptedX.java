/// Suyeon Hong
/// 10/23/2018
/// CSE 002 - 210
/// HW 06
/// To practice using nested loops by creating patterns
import java.util.Scanner; // importing Scanner

public class EncryptedX { // class
    public static void main(String args[]) { // main method
        Scanner myScanner = new Scanner(System.in);

        int i;
        int j;
        int n;
        System.out.println("Enter an interger between 1 and 100:");

        n = -1;
        while (n < 0 || n > 100) { // checks to see if the range is between 0 and 100
            while (!myScanner.hasNextInt()) {
                myScanner.next(); // if the input is not an integer
                System.out.println("That's not an integer; error entry");
            }
            System.out.println("That's out of range. Enter a value again");
            n = myScanner.nextInt();
        }
        for (i = 0; i <= n; i++) { // number of rows
            for (j = 0; j <= n; j++) { // number of columns
                if (i == j) { // first diagonal line (spaces) (left to right)
                    System.out.print(" "); // printing spaces
                } else if (j == (n - (i))) { // second diagonal line (spaces) (right to left)
                    System.out.print(" "); // printing spaces
                } else {
                    System.out.print("*"); // printing stars
                }
            }
            System.out.println(); // moves to the next line after the pattern in each row is completed
        }
    } // ends main method
} // ends class
