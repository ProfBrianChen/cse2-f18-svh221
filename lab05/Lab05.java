/// Suyeon Hong
/// 10/03/2018
/// CSE 002 - 210
/// lab05 Lab05
/// To practice using switch statements, and Math.random(). 

import java.util.Scanner; // importing Scanner
public class Lab05 {  // class
  public static void main(String args[]) { // main method
    Scanner myScanner =  new Scanner ( System.in );
    // 1. course Number (int)
    // 2. the department Name (String)
    // 3. the number of times the course meet in a week (int)
    // 4. the time of day the  course begins (1500 (3PM))
    // 5. the instructor name (String)
    // 6. the number of students in course (int)
    
    // asks for these values in this order
    // use two do-while, two while
    int course = 0; // initializing
    String depName = ""; 
    int numMeet = 0;
    int time = 0;
    String instructorName = "";
    int numStudents = 0;
    
    boolean truth = true;  
    System.out.println("Please enter the course number"); // asks for an input
      while (truth){ // while it's true
        if(!myScanner.hasNextInt()){ // if integer wasn't entered
          myScanner.next(); // clears out
          System.out.println("That's not an integer; error entry"); // asks the users to put down an integer
        }
        else { // if integer was put
          course = myScanner.nextInt(); // scans for the integer
          truth = false; // ends
        }
      }
    
    truth = true;
    System.out.println("Please enter the department name");
      while (truth){
        if(!myScanner.hasNext()){
          myScanner.next();
          System.out.println("That's not a string; error entry");
      }
        else {
        depName = myScanner.next();
        truth = false;
      }
    }

    truth = true;
    System.out.println("Please enter the number of times the course meet in a week");
      while (truth){
        if(!myScanner.hasNextInt()){
        myScanner.next();
        System.out.println("That's not an integer; error entry");
      }
        else {
        numMeet = myScanner.nextInt();
        truth = false;
        }
      }
    
    truth = true;
    System.out.println("Please enter the time of the day the course begins");
     while (truth){
        if(!myScanner.hasNextInt()){
        myScanner.next();
        System.out.println("That's not an integer; error entry");
     }
        else {
        time = myScanner.nextInt();
        truth = false;
        }
      }
    
    truth = true;
    System.out.println("Please enter the name of your instructor");
    while (truth){
          if(!myScanner.hasNext()){
          myScanner.nextLine();
          System.out.println("That's not a valid entry; error entry"); 
      }
        else {
        instructorName = myScanner.next();
        truth = false;
      }
    }
        truth = true;
        System.out.println("Please enter the number of students");
        while (truth){
          if(!myScanner.hasNextInt()){
          myScanner.next();
          System.out.println("That's not an integer; error entry");
        }
          else {
             numStudents = myScanner.nextInt();
             truth = false;
          }
        }
   
        System.out.println("Course Name:" + course); // displays course name
        System.out.println("Department Name:" + depName); // displays department name
        System.out.println("The number of times of the meeting:" + numMeet); // displays the number of times of meeting
        System.out.println("Time of the meeting:" + time); // displays the time of the meeting
        System.out.println("The name of the instructor's name:"+ instructorName); // displays the instructor name
        System.out.println("The number of students:"+ numStudents); // displays the number of students

      } // end main
      } // end class