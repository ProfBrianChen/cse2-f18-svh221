///
/// Suyeon Hong 
/// 09/04/18
/// CSE 002
/// HW #1 Welcome Message 
/// Writing a Java program that displays a welcome message, using my Lehigh ID and signature. 

public class WelcomeClass {
  
  public static void main(String arg[]) {
    // prints the WELCOME message
    System.out.println("  ----------- ");
    System.out.println("  | WELCOME | ");
    System.out.println("  ----------- ");
    System.out.println("  ^  ^  ^  ^  ^  ^ ");
    // in order to print '\' need to use \\
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    // Prints Lehigh Student ID
    System.out.println("<-S--V--H--2--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
    System.out.println("  v  v  v  v  v  v  ");
    // Prints a tweet-length autobiographic statement
    System.out.println(" Hello, my name is Suyeon Hong and I'm going to be a sophomore at Lehigh, majoring in ISE and minoring in Marketing. ");
  }
}