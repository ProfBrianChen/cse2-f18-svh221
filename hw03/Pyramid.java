/// Suyeon Hong
/// 9/18/2018
/// CSE 002 - 210
/// hw03 Pyramid
/// To practice enabling user to input data and to practice in calculating arithmetic operations.
import java.util.Scanner;
public class Pyramid { // class
  public static void main(String arg[]) { // main method
    
    Scanner myScanner = new Scanner (System.in); 
    System.out.print("The square side of the pyramid is: "); // asks users for the length of the base of the pyramid 
    double baseLength = myScanner.nextDouble(); // accepting the user input
    System.out.print("The height of the pyramid is: "); // asks users for the height of the pyramid
    double height = myScanner.nextDouble(); // accepting the user input
    
    double volume; // initializing
    double baseArea; // initializing 
    
    baseArea = Math.pow(baseLength,2); // squaring the base Length; base Length^2
    
    volume = (baseArea*height)/3; // calculates the volume of the pyramid
    System.out.println("The volume inside the pyramid is: " + volume); // states the volume of the pyramid
  } // end of main method
} // end of class