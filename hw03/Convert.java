/// Suyeon Hong
/// 9/18/2018
/// CSE 002 - 210
/// hw03 Convert
/// To practice enabling user to input data and to practice in calculating arithmetic operations.
import java.util.Scanner;
public class Convert { // class
  public static void main(String arg[]) { // main method
    
    Scanner myScanner = new Scanner ( System.in ); // scanner, accepting input
    System.out.print("Enter the affected area in acres: "); // asks users for the affected area in acres
    double Area = myScanner.nextDouble(); // accepting the user input
    System.out.print("Enter the rainfall in the affected area: "); // asks users for the amount of the rainfall in the affected area in inches
    double rainFall = myScanner.nextDouble(); // accepting the user input
    
    double rainInGallons; // initializing
    
    double convertToGallons = 27154.2857; // initializing; conversion factor; in/acre to gallons
    rainInGallons = Area * rainFall * convertToGallons; // converts the amount of the rainfall in the affected area to gallons
    double gallonsToCubicMiles = 9.08169e-13;  // initializing; conversion factor; gallons to cubic miles
    double rainInCubicMiles; // initializing
 
    rainInCubicMiles = rainInGallons * gallonsToCubicMiles; // converts the amount of the rainfall in the affected area to cubic miles
   System.out.println(rainInCubicMiles + " cubic miles"); // displays the answer; rain in cubic miles
  } //end of main method
} // end of class