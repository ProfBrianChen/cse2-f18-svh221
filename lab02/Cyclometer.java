// Suyeon Hong
// 09/05/18
// CSE 002 - 210
// Lab 02
// My bicycle cyclometer - to understand how java program stores numerical values in variables and how it computes those variables.

public class Cyclometer {
// main method for java program
   public static void main(String arg[]) {
     // input data
     int secsTrip1 = 480; // number of seconds for trip1
     int secsTrip2 = 3220; // number of seconds for trip2
     int countsTrip1 = 1561; // number of counts for trip1
     int countsTrip2 = 9037; // number of counts for trip2
     
     double wheelDiameter = 27.0; // in inches
     double PI = 3.14159; // pi 
     double feetPerMile = 5280; // 5280 feet per mile
     double inchesPerFoot = 12; // 12 inches per foot
     double secondsPerMinute = 60; // 60 seconds in a minute
     double distanceTrip1, distanceTrip2, totalDistance; // output variables
     
     // Prints the output of data
     System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
     System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
    
     // Calculating trip 1&2's distances with conversion 
     distanceTrip1=countsTrip1*wheelDiameter*PI;
     distanceTrip1/=inchesPerFoot*feetPerMile;
     distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
     totalDistance=distanceTrip1+distanceTrip2;
     
     // prints out the output data in sentences 
     System.out.println("Trip 1 was " +distanceTrip1+" miles");
     System.out.println("Trip 2 was " +distanceTrip2+" miles");
     System.out.println("Total distance was "+totalDistance+" miles");
    
     
} // end of main method
} // end of class


