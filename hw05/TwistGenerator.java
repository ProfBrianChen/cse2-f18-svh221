/// Suyeon Hong
/// 10/05/2018
/// CSE 002 - 210
/// Hw05 Twist Generator
/// To get familiar with loops, a critical piece of syntax that is essential for many programming languages.
import java.util.Scanner; // import scanner
public class TwistGenerator { // class starts
    public static void main (String arg[]) { // main method
        Scanner myScanner = new Scanner ( System.in ); 
        int length = 1; // initializing
        int i;

        

        System.out.println("What is the length?"); // asks the users for input
        while (!myScanner.hasNextInt()) { // while the input is not an integer
            System.out.println("That is not a correct input. Please enter an integer again.");
            myScanner.next(); // clears out
        }
        while (myScanner.hasNextInt()) { // while the input is an integer
            if (length < 0){ // if the input is negative
            System.out.println("The length cannot be negative nor zero.  Please enter a new value again.");
            myScanner.next(); // clears out
        }
        else {
            length = myScanner.nextInt();
            int remainder = length%3; // the measurement of how incomplete the line is
            int section = length/3; // the number of complete sections
            for (i = 0; i < section; i++) {
               System.out.print("\\ /");
            }
            if (remainder == 1){ 
                System.out.println("\\"); // adds '\' to the end of the row if the remainder from input/3 is 1
            }
            if (remainder == 2){
                System.out.println("\\ "); // // adds '\'+" " if the remainder from input/3 is 2
            }
        
            for (i = 0; i < section; i++) {
               System.out.print(" X ");
            }
            System.out.println(""); // finishes the row 

            for (i = 0; i < section; i++) {
               System.out.print("/ \\");
            }
            if (remainder == 1){
               System.out.println("/");
            }
            if (remainder == 2){
               System.out.println("// ");
            } // main method ends
        } // class ends
    }
  }
}

        

  

