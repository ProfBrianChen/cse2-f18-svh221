
/// Suyeon Hong
/// 11/12/2018
/// CSE 002 - 210
/// HW 8 - Shuffling
/// To practice using Arrays

import java.util.Scanner;

public class hw08 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        // suits club, heart, spade or diamond
        String[] suitNames = { "C", "H", "S", "D" };
        String[] rankNames = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A" };
        String[] cards = new String[52];
        String[] hand = new String[5];
        int numCards = 5;
        int again = 1;
        int index = 51;
        for (int i = 0; i < 52; i++) {
            cards[i] = rankNames[i % 13] + suitNames[i / 13];
            System.out.print(cards[i] + " ");
        }
        System.out.println();
        printArray(cards);
        shuffle(cards);
        printArray(cards);
        while (again == 1) {
            hand = getHand(cards, index, numCards);
            printArray(hand);
            index = index - numCards;
            System.out.println("Enter a 1 if you want another hand drawn");
            again = scan.nextInt();
        }
        scan.close(); 
    }

    public static void printArray(String[] list) {
        for (int i = 0; i < list.length; i++) { // looping through the entire list
            if (list[i] != null) { // if the value exists
                System.out.print(list[i] + " ");
            }
        }
        System.out.println();
    }

    public static void shuffle(String[] list) {
        int random = 1;
        for (int i = 0; i < 60; i++) {
            random = (int) (Math.random() * list.length - 1) + 1; // generates random number within the list bounds
            String temp = list[0];
            list[0] = list[random]; // swapping 
            list[random] = temp; // swapping
        } 
    }

    public static String[] getHand(String[] list, int index, int numCards) {
        // everything
        String[] output = null;
        if (index > 0 && numCards > 0) { // ensures valid index (positive) 
            output = new String[numCards]; 
            int handPos = 0;
            for (int i = index; i > index - numCards; i--) { // loops backwards from the end of the list
                if (i > 0) {
                    output[handPos] = list[i]; // fills the hand with the list value
                    handPos++;
                }
            }
            return output;
        } else if (numCards == 0) {
            return output;
        }

        else if (numCards < 0) {
            return output;
        }

        else { // creating a new deck of cards
            String[] suitNames = { "C", "H", "S", "D" };
            String[] rankNames = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A" };
            String[] cards = new String[52];
            for (int i = 0; i < 52; i++) {
                cards[i] = rankNames[i % 13] + suitNames[i / 13];
            }
            return cards;
        }
    }
}
