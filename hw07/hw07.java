/// Suyeon Hong
/// 11/7/2018
/// CSE 002 - 210
/// HW 7 - Word Tools
/// To practice using methods

import java.util.Scanner;
public class hw07 { // class starts

    public static String sampleText() { // method 1
        String input;
        Scanner scnr = new Scanner(System.in);

        System.out.println("Enter a sample text:"); // asks for the user input
        input = scnr.nextLine(); // scans for the input

        System.out.print("You entered: " + input); // prints the input again
        System.out.println(" ");

        return input;
    }

    public static void printMenu() { // method 2
        System.out.println("MENU");
        System.out.println("c - Number of non-whitespace characters");
        System.out.println("w - Number of words");
        System.out.println("f - Find text");
        System.out.println("r - Replace all !'s");
        System.out.println("s - Shorten spaces");
        System.out.println("q - Quit");
    }

    // method 3
    public static int getNumOfNonWSCharacters(String input) {
        int i;
        int counter = 0;
        for (i = 0; i <= input.length() - 1; i++) {
            if (!Character.isWhitespace(input.charAt(i))) { // if the character at that spot is not a space
                counter++; // increments
            }
        }
        return counter;
    }

    // method 4
    public static int getNumOfWords(String input) {
        int i;
        int counter = 0;
        boolean isWord = false;
        int x = input.length() - 1;

        for (i = 0; i <= input.length() - 1; i++) {
            if (Character.isLetter(input.charAt(i)) && i != x) {
                isWord = true;
            } else if (!Character.isLetter(input.charAt(i)) && isWord) {
                counter++;
                isWord = false;
            } else if (Character.isLetter(input.charAt(i)) && i == x)
                counter++;
        }
        return counter;
    }

    // method 5
    public static int findText(String find, String input) {

        int counter = 0; // counts(increments) how many matching words there are
        int word = 0; // What letters of the word did we find so far

        for (int i = 0; i <= input.length() - 1; i++) {
            char compare = input.charAt(i);
            char match = find.charAt(word);

            // not matching
            if (match != compare) {
                word = 0;
            }
            // matching
            else {
                word++;
            }
            // increments
            if (word == find.length()) {
                counter++;
                word = 0;
            }
        }

        return counter;
    }

    // method 6
    public static String replaceExclamation(String input) {
        String input2 = input.replace('!', '.'); // replacing ! with .
        return input2;
    }

    // method 7
    public static String shortenSpace(String sh) {
        String returnValue = "";

        for (int i = 0; i <= sh.length() - 1; i++) {
            char letter = sh.charAt(i);

            if (letter == ' ') {
                returnValue += letter;

                while (letter == ' ') {
                    i++;
                    letter = sh.charAt(i);
                }
            }
            returnValue += letter;
        }
        return returnValue;
    }
    // main
    public static void main(String[] args) {
        String input = sampleText(); // calls method 1
        boolean fact = true;

        while (fact) {

            printMenu(); // calls method 2

            Scanner scan = new Scanner(System.in);
            String choice = scan.next(); // scans the letter that the user has put

            if (choice.equals("q")) { // if q is entered
                fact = false; // exit the program
            }

            else if (choice.equals("c")) {
                int counter = getNumOfNonWSCharacters(input); // calls method 3
                System.out.println("Number of non-whitespaces characters: " + counter);
                fact = false;
            }

            else if (choice.equals("w")) {
                int counter = getNumOfWords(input); // calls method 4
                System.out.println("Number of words: " + counter);
                fact = false;
            }

            else if (choice.equals("f")) {
                System.out.println("Enter a word or phrase to be found: ");
                String find = scan.next();
                int counter = findText(find, input); // calls method 5
                System.out.println(find + " instances: " + counter);
                fact = false;
            }

            else if (choice.equals("r")) {
                System.out.println(replaceExclamation(input)); // calls method 6
                fact = false;
            }

            else if (choice.equals("s")) {
                String redo = shortenSpace(input); // calls method 7
                System.out.println(redo);
                 fact = false;
            }

            else {
                System.out.println("Invalid input.");
            }
        }
    } // main method ends

} // class ends
