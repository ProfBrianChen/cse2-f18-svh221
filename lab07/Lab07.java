/// Suyeon Hong
/// 11/06/2018
/// CSE 002 - 210
/// Lab07
/// To practice using methods

import java.util.Random;
import java.util.Scanner;

public class Lab07{ // class starts

  // Phase 0 
  public static String adj(String adj){
    Random randomGenerator = new Random();
     adj = "";
    int randomInt = randomGenerator.nextInt(10); // randomly generates adjective
    switch (randomInt){  
      case 0:
        adj = "soft" ;
          break;
      case 1:
        adj = "excited";
        break;
      case 2:
        adj = "sad";
        break;
      case 3:
        adj = "cool";
        break;
      case 4:
        adj = "fast";
        break;
      case 5:
        adj = "strange";
        break;
      case 6:
        adj = "successful";
        break;
      case 7:
        adj = "smart";
        break;
      case 8:
        adj = "crazy";
        break;
      case 9:
        adj = "happy";
        break;    
    }
    return adj;
  }

  public static String subj(String subj){
    Random randomGenerator = new Random();
     subj = "";
    int randomInt = randomGenerator.nextInt(10); //randomly generates subject
        switch (randomInt){
      case 0:
        subj = "student" ;
          break;
      case 1:
        subj = "roommate";
        break;
      case 2:
        subj = "friend";
        break;
      case 3:
        subj = "cloud";
        break;
      case 4:
        subj = "water";
        break;
      case 5:
        subj = "grade";
        break;
      case 6:
        subj = "class";
        break;
      case 7:
        subj = "desk";
        break;
      case 8:
        subj = "calculator";
        break;
      case 9:
        subj = "dolphin";
        break;    
    }
    return subj;
  }

  public static String verb(String verb){
        Random randomGenerator = new Random(); //randomly generates verb
     verb = "";
    int randomInt = randomGenerator.nextInt(10);
        switch (randomInt){
      case 0:
        verb = "swam" ;
          break;
      case 1:
        verb = "ran";
        break;
      case 2:
        verb = "slept";
        break;
      case 3:
        verb = "poked";
        break;
      case 4:
        verb = "walked";
        break;
      case 5:
        verb = "wrote";
        break;
      case 6:
        verb = "crushed";
        break;
      case 7:
        verb = "cried";
        break;
      case 8:
        verb = "threw";
        break;
      case 9:
        verb = "killed";
        break;    
    }
    return verb;
    
  }

  public static String obj(String obj){
            Random randomGenerator = new Random();
     obj = "";
    int randomInt = randomGenerator.nextInt(10); //randomly generates object
        switch (randomInt){
      case 0:
        obj = "car" ;
          break;
      case 1:
        obj = "soda";
        break;
      case 2:
        obj = "lipstick";
        break;
      case 3:
        obj = "fork";
        break;
      case 4:
        obj = "coffee";
        break;
      case 5:
        obj = "test";
        break;
      case 6:
        obj = "turtle";
        break;
      case 7:
        obj = "pillow";
        break;
      case 8:
        obj = "jacket";
        break;
      case 9:
        obj = "spoon";
        break;    
    }
    return obj;
  }
  public static void main(String [] args){ // main method starts

    // Phase 1

    Scanner input = new Scanner(System.in);
    String adjective = "";
    String adjective2 = "";
    String adjective3 = "";
    String subject = "";
    String verb = "";
    String nounObject = "";

     adjective = adj(adjective);
     adjective2 = adj(adjective);
     adjective3 = adj(adjective);
     subject = subj(subject);
     verb = verb(verb);
     nounObject = obj(nounObject);

     System.out.println("The " + adjective + " " + adjective2 + " " + subject + " " + verb + " " + "the " + adjective3 + " " + nounObject + "."); // Prints the first sentence

    // Phase 2

    String subject2 = "";
    String verb2 = "";
    String verb3 = "";
    String adjective4 = "";
    String adjective5 = "";
    String adjective6 = "";
    String adjective7 = "";
    String nounObject2 = "";
    String nounObject3 = "";
    String nounObject4 = "";
    String nounObject5 = "";
    String nounObject6 = "";

    verb2 = verb(verb2);
    verb3 = verb(verb3);
    adjective4 = adj(adjective4);
    adjective5 = adj(adjective5);
    adjective6 = adj(adjective6);
    adjective7 = adj(adjective7);
    nounObject2 = obj(nounObject2);
    nounObject3 = obj(nounObject3);
    nounObject4 = obj(nounObject4);
    nounObject5 = obj(nounObject5);
    nounObject6 = obj(nounObject6);

        
    System.out.println("This " + subject + " was " + adjective4 + " " + adjective5 + " to " + adjective6 + " " + nounObject2 + "."); // second sentence
    System.out.println("It " + verb2  + " the " + nounObject3 + " to " + verb3 + " " + nounObject4 + " at the " + adjective7 + " " + nounObject5 + "."); // third sentence
    System.out.println("That " + subject + " " + verb3 + " its " + nounObject6 + "!"); // fourth sentence
   
   
   // final method

    String sent1 = "";
    String sent2 = "";
    String sent3 = "";
    String sent4 = "";
    String sent5 = "";
    
    sent1 = sentence(sent1);
    sent2 = sentence(sent2);
    sent3 = sentence(sent3);
    sent4 = sentence(sent4);
    sent5 = sentence(sent5);
    
    System.out.println(sent1 + sent2 + sent3 + sent4 + sent5);
    
    System.out.print("Would you like another sentence? 0 for yes, 1 for no. ");
  
    int response = input.nextInt();
    if (response == 0){
      adjective = adj(adjective);
      adjective2 = adj(adjective);
      adjective3 = adj(adjective);
      adjective4 = adj(adjective);
      adjective5 = adj(adjective);
      adjective6 = adj(adjective);

      subject = subj(subject);
      verb = verb(verb);
      verb2 = verb(verb);
      verb3 = verb(verb);

      nounObject = obj(nounObject);
      nounObject2 = obj(nounObject);
      nounObject3 = obj(nounObject);
      nounObject4 = obj(nounObject);
      nounObject5 = obj(nounObject);
      nounObject6 = obj(nounObject);


      System.out.println("This" + " " + subject + " was" + adjective4 + " " + adjective5 + " to " + adjective6 + " " + nounObject2 + ".");
      System.out.println("It " + verb2  + " the " + nounObject3 + " to " + verb3 + " " + nounObject4 + "at the " + adjective7 + " " + nounObject5 + ".");
      System.out.println("That " + subject + " " + verb3 + " its " + nounObject6 + "!");  
    }
    else if (response == 1){
     return;
    }
    else {
    System.out.println("Invalid number. Enter again.");
    response = input.nextInt();
    }
  }

  public static String sentence(String sentence){ 
    Random randomGenerator = new Random();
        sentence = "";
    int randomInt = randomGenerator.nextInt(10); //randomly generates sentence
           switch (randomInt){
      case 0:
        sentence = "I'm trying my best. " ;
          break;
      case 1:
         sentence = "I don't know where I will be living next year. ";
        break;
      case 2:
         sentence = "This lab is cool but challenging. ";
        break;
      case 3:
        sentence = "I want to get an internship this Summer. ";
        break;
      case 4:
         sentence = "Why does Calculus exist? ";
        break;
      case 5:
        sentence = "I need coffee right now. ";
        break;
      case 6:
        sentence = "My roommate and I are bestfriends. ";
        break;
      case 7:
        sentence = "I am frustrated. ";
        break;
      case 8:
        sentence = "I am also hungry. ";
        break;
    }
    return sentence;
  } // end main method
} // end class