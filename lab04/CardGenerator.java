/// Suyeon Hong
/// 9/19/2018
/// CSE 002 - 210
/// lab04 Card Generator
/// To practice using if statements, switch statements, and Math.random(). 

public class CardGenerator { // class
  public static void main(String arg[]) { // main method
    
   int number = (int)((Math.random()*52)+1); // generates random number that's between 1 to 52 (inclusive)
    String suit = "";
    String identity = "";
    
    if (number >= 1 && number <= 13){ // Card 1~13 represents diamonds
      suit = "diamonds";
    }
    else if (number > 13 && number <= 26){ // Card 13 ~ 26 represents clubs
      suit = "clubs";
    }
    
    else if (number > 26 && number <= 39){ // Card 26 ~ 39 represents hearts
      suit = "hearts";
    }

    else if (number > 39 && number <= 52){ // Card 39 ~ 52 represents spades
      suit = "spades";
    }
    
    switch (number % 13) {
      case 1:
        identity = "Ace"; // if the remainder is 0 (ex. 14 % 14 = 0), represents Ace 
        break;
      case 2:
        identity = "2"; // if the remainder is 1 (ex. 15 % 14 = 0), represents 2 
        break;
      case 3:
        identity = "3"; // if the remainder is 2 (ex. 16 % 14 = 0), represents 3 
        break;
      case 4:
        identity = "4"; // if the remainder is 3 (ex. 17 % 14 = 0), represents 4 
        break;
      case 5:
        identity = "5"; // if the remainder is 4 (ex. 18 % 14 = 0), represents 5 
        break;
      case 6:
        identity = "6"; // if the remainder is 5 (ex. 19 % 14 = 0), represents 6 
        break;
      case 7:
        identity = "7"; // if the remainder is 6 (ex. 20 % 14 = 0), represents 7 
        break;
      case 8:
        identity = "8"; // if the remainder is 7 (ex. 21 % 14 = 0), represents 8 
        break;
      case 9:
        identity = "9"; // if the remainder is 8 (ex. 22 % 14 = 0), represents 9 
        break; 
      case 10:
        identity = "10"; // if the remainder is 9 (ex. 23 % 14 = 0), represents 10 
        break;
      case 11:
        identity = "Jack"; // if the remainder is 10 (ex. 24 % 14 = 0), represents Jack
        break;
      case 12:
        identity = "Queen"; // if the remainder is 11 (ex. 25 % 14 = 0), represents Queen
        break;
      case 0:
        identity = "King"; // if the remainder is 12 (ex. 26 % 14 = 0), represents King 
        break;
      }
    System.out.println(number); // Prints out the random number that was generated
    System.out.println("You picked the" + " " + identity + " " + "of" + " " + suit); // prints the result
    } // Main method ends
} // class ends