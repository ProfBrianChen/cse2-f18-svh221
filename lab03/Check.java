/// Suyeon Hong
/// 9/12/2018
/// CSE 002 - 210
/// lab03 Check
/// To demonstrate the understanding of how you can get input from the user and use that data to perform basic computations.  

import java.util.Scanner;
public class Check { // class
  public static void main(String arg[]) { // main method
    
    Scanner myScanner = new Scanner ( System.in ); // scanner, accepting input
    
    System.out.print("Enter the original cost of the check in the form xx.xx: "); // asks for the original cost of the check to the user
    double checkCost = myScanner.nextDouble(); // accepting the user input
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); // asks for the percentage tip that the user wishes to pay
    double tipPercent = myScanner.nextDouble(); // accepting the user input
    tipPercent /= 100; // Converting percentage value into a decimal value
   
    System.out.print("Enger the number of people who went out to dinner: "); // asks for the number of people who had the dinner
    int numPeople = myScanner.nextInt(); // accepting the user input
    
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies; // Whole dollar amount, dropping the decimal fraction dollars.  The same goes to dimes, pennies
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    dollars = (int)costPerPerson;
    dimes = (int)(costPerPerson * 10) % 10; // % returns the remainder
    pennies = (int)(costPerPerson * 100) % 10; // % returns the remainder
    
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); // telss the user the amount of each person owes
  } // end of main method 
} // end of class