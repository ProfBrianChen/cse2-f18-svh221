//CSE2
//lab08
//11/7/18
//Suyeon Hong
//To practice using one-dimensional arrays

import java.util.Random;

public class lab08 { // class starts
  public static void main(String[] args) {
    Random randomGenerator = new Random();
    int[] arrayOne = new int[100]; // creating the first array that has a size of 100
    int[] arrayTwo = new int[100]; // creating the second array that also has a size of 100
    
    System.out.print("The Array 1 holds the following integers: ");
    int num = 0;
    int i = 0;
    for (i = 0; i < arrayOne.length; i++) { // looping through the entire list
      arrayOne[i] = randomGenerator.nextInt(100); // filling in one of the arrays with randomized integers in the range of 0 to 99
      num = arrayOne[i]; 
      arrayTwo[num]++; // counting the number of occurrences of each number in the first array in second array
      System.out.print(arrayOne[i] + " ");
    }
    System.out.println(" ");
    
    for (i = 0; i < arrayTwo.length; i++) { 
      if (arrayTwo[i] > 0) { // if positive number
        System.out.println(i + " occurs " + arrayTwo[i] + " time(s).");
      }
    }
  } // class ends 
} // main ends