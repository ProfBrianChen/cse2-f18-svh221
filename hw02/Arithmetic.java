/// Suyeon Hong
/// 9/11/2018
/// CSE 002 - 210
/// HW #2 Arithmetic
/// To practice manipulating data stored in variables by running calculations and printing the numerical output value.

public class Arithmetic {
  public static void main(String arg[]) {
    // Number of pairs of pants
    int numPants = 3;
    // Cost per pair of pants
    double pantsPrice = 34.98;
    
    // Number of sweatshirts
    int numShirts = 2;
    // Cost per shirt
    double shirtPrice = 24.99;
    
    // Number of belts
    int numBelts = 1;
    // cost per belt
    double beltCost = 33.99;
    
    // the tax rate
    double paSalesTax = 0.06;
    
    // output variables
    double totalCostOfPants,totalCostOfShirts,totalCostOfBelts;
    double SalesTaxOnPants,SalesTaxOnShirts,SalesTaxOnBelts;
    double totalCostBeforeTax,totalSalesTax,totalTransaction;
    
    // total cost of each item
  totalCostOfPants=numPants*pantsPrice;
  totalCostOfShirts=numShirts*shirtPrice;
  totalCostOfBelts=numBelts*beltCost;
    
    // sales tax on each item
  SalesTaxOnPants = (int)((totalCostOfPants * paSalesTax)*100)/100.0;
  SalesTaxOnShirts = (int)((totalCostOfShirts * paSalesTax)*100)/100.0;
  SalesTaxOnBelts = (int)((totalCostOfBelts * paSalesTax)*100)/100.0;
  
    // total cost before tax
  totalCostBeforeTax = (int)((totalCostOfPants + totalCostOfShirts + totalCostOfBelts)*100)/100.0;
    // total sales tax
  totalSalesTax = (int)((totalCostBeforeTax * paSalesTax)*100)/100.0;
    // total amount paid 
  totalTransaction = (int)((totalCostBeforeTax + totalSalesTax)*100)/100.0;
    
    // Prints/displays the outputs/Prices
  System.out.println("Total cost of pants = " +totalCostOfPants);
  System.out.println("Total cost of shirts = " +totalCostOfShirts);
  System.out.println("Total cost of belts = " +totalCostOfBelts);
    
  System.out.println("Sales tax on pants= " +SalesTaxOnPants);
  System.out.println("Sales tax on shirts= " +SalesTaxOnShirts);
  System.out.println("Sales tax on belts= " +SalesTaxOnBelts);
    
  System.out.println("Total cost of items without sales tax= " +totalCostBeforeTax);
  System.out.println("Total sales tax= " +totalSalesTax);
  System.out.println("Total transaction amount= " +totalTransaction);
    
  } // end of main method
  } // end of class
