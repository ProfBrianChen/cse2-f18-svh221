
/// CSE 002 -210
/// HW9
/// Remove Elements
/// Suyeon Hong
/// 11/26/2018

import java.util.Scanner;

public class RemoveElements { // class
    public static void main(String[] arg) { // main
        Scanner scnr = new Scanner(System.in);
        int num[] = new int[10];
        int newArray1[];
        int newArray2[];
        int index, target;
        String answer = "";
        do {
            System.out.println("Random input 10 ints [0-9]");
            num = randomInput(num); // generate the random number for each element
            String out = "The original array is:";
            out += listArray(num);
            System.out.println(out);

            System.out.print("Enter the index to delete ");
            index = scnr.nextInt();
            newArray1 = delete(num, index);
            String out1 = "The output array is ";
            out1 += listArray(newArray1); // return a string of the form "{2, 3, -9}" System.out.println(out1);
            System.out.println(out1);

            System.out.print("Enter the target value ");
            target = scnr.nextInt();
            newArray2 = remove(num, target);
            String out2 = "The output array is ";
            out2 += listArray(newArray2); // return a string of the form "{2, 3, -9}" System.out.println(out2);
            System.out.println(out2);

            System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
            answer = scnr.next();
        } while (answer.equals("Y") || answer.equals("y"));
    }

    public static String listArray(int num[]) {
        String out = "{";
        for (int j = 0; j < num.length; j++) {
            if (j > 0) {
                out += ", ";
            }
            out += num[j];
        }
        out += "} ";
        return out;
    }

    public static int[] randomInput(int num[]) { // random method
        int i = 0;
        for (i = 0; i < num.length; i++) {
            num[i] = (int) (Math.random() * 10); // randomly generating numbers between 0 to 9
        }
        return num;
    }

    public static int[] delete(int[] list, int pos) { // delte method
        int[] list2 = new int[9]; // smaller array
        int i;

        for (i = pos; i < list.length - 1; i++) { // after index is found, starts to replace the elements, starting from
                                                  // that index
            list[i] = list[i + 1];
        }
        for (i = 0; i < list2.length; i++) { // setting up list2
            list2[i] = list[i];
        }
        return list2;
    }

    public static int[] remove(int[] list, int target) {
        int count = 0;

        for (int i = 0; i < list.length - 1; i++) {
            if (list[i] == target) { // if target integer is found
                count++;
            }
        }

        if (count == 0) { // if target integer is not found
            return list; // original array is printed
        }

        int[] rem = new int[list.length - 1 - count];

        int counter = 0;
        for (int i = 0; i < list.length - 1; i++) {
            if (list[i] != target) {
                rem[counter] = list[i];
                counter++;
            } else {
                continue;
            }
        }
        return rem;
    }
}
