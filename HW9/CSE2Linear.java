
/// Suyeon Hong
/// 11/24/2018
/// CSE 002 - 210
/// HW 9 - Searching
/// Program 1
/// To practice using arrays and in searching single dimensional arrays

import java.util.Scanner;
import java.util.Random;

public class CSE2Linear {
        public static void main(String[] args) { // main method
        Scanner scnr = new Scanner(System.in);
        final int num_elements = 15; // number of elements
        int currInt;
        int i; // loop index
        int[] grade = new int[15];
        int key;
        int key2;
        int keyIndex;
        int keyIndex2;
        int j;
        // asking users to enter 15 integers
        System.out.println("Enter 15 ascending ints for final grades in CSE2:");

        i = 0;
        for (j = 0; j < 15; j++) { // accepts 15 integers
            boolean truth = true;
            while (truth) {
                if (!scnr.hasNextInt()) { // if the input is not an integer
                    System.out.println("Error 1: Invalid input. Please enter only integers.");
                    scnr.next();
                } else {
                    currInt = scnr.nextInt(); // accepts the input 
                    if (currInt > 100 || currInt < 0) { // checks to see if it's out of the boundry
                        System.out.println("Error 2: Out of bounds. Please enter integers between 0 and 100.");
                        scnr.next();
                        continue;
                    } else {
                        grade[i] = currInt; // stores the value into the array
                        if (i > 0 && grade[i] < grade[i - 1]) { // checks to see if it's in an ascending order
                            System.out.println("Error 3: Please enter integers in ascending order.");
                            continue;
                        } else {
                            truth = false; // everything is okay, so moving on
                            i++; // increments so that it accepts 15 correct inputs
                        }
                    }
                }
            }
        }
        
        for (j = 0; j < 15; j++) {
            System.out.print(grade[j] + " ");
        }
        System.out.println("");

        System.out.print("Enter a grade to search for: ");
        key = scnr.nextInt(); // accepting the user input

        keyIndex = binary(grade, j, key); // calls the binary method 

        System.out.println("Scrambled:"); 
        scramble(grade); // calls the scramble method

        System.out.println("Enter a grade to search for: ");
        key2 = scnr.nextInt(); // accepting the user input

        keyIndex2 = linear(grade, j, key2); // calling the linear method

        if (linear(grade, j, key2) > 0) { // if the value that the user is looking for is found
            System.out.println(key2 + " was found with " + linear(grade, j, key2) + " iterations.");
        }
        else // if not found
        { 
            System.out.println(key2 + " was not found with " + linear(grade, j, key2) + " iterations.");
        }
        
    }

    public static int binary(int grade[], int j, int key) {
        int mid;
        int low;
        int high;
        int counter = 1;
        low = 0;
        high = j - 1;

        while (high >= low) {
            mid = (high + low) / 2; // checks the number in the "middle" first
            if (grade[mid] < key) { // if the user's input is bigger
                low = mid + 1; 
            } else if (grade[mid] > key) {
                high = mid - 1;
            } else {
                System.out.println(key + " " + "was found with " + counter + " iterations.");
                return counter;
            }
            counter++;
        }
        System.out.println(key + " was not found with " + counter + " iterations.");
        return counter; // not found
    }

    public static void scramble(int[] grade) {
        for (int i = 0; i < grade.length - 1; i++) {
            int random = (int) (Math.random() * grade.length - 1) + 1; // generates random number within the grade //                                                       // bounds
            int temp = grade[i]; // temporarily storing 
            grade[i] = grade[random]; // swapping
            grade[random] = temp; // swapping
        }
        
        for (int i=0; i < grade.length; i++){
            System.out.print(grade[i] + " "); // printing the scrambled array
        }
        System.out.println("");
      
    }

    public static int linear(int grade[], int j, int key2) {
        int count = 1;
        for (int i = 0; i < grade.length - 1; i++) { // compares every element in the array with the key
            if (grade[i] == key2) {
                return count;
            }
            count++;
        }
        return count;
    }
}
