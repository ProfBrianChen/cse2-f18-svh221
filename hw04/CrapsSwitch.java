/// Suyeon Hong
/// 9/25/2018
/// CSE 002 - 210
/// hw04 CrapsSwitch
/// To practice using switch statements, and Math.random(). 

import java.util.Scanner; // importing Scanner
public class CrapsSwitch {  // class
  public static void main(String arg[]) { // main method
    Scanner myScanner =  new Scanner ( System.in );  //scanner, accepting input
    System.out.println("If you want to randomly cast dice, type a. If you'd like to state the two dice, type b: "); // asks the user if they'd like to randomly cast the dice/ or state the two dice
    
    String choice = ""; // initializing
    
    choice = myScanner.nextLine();
   
    int dice1 = 7; // initializing
    int dice2 = 7; // initializing
    int doA = 0; // initializing
    int doB = 0; // initializing
    
 
    switch (choice) {
      
      case "a": // if the user chooses randomly generating method
        doA = 1; // if true (if the user has put down "a")
        dice1 = (int)(Math.random() * 5) + 1; // dice 1: 1 to 6 is randomly generated
        dice2 = (int)(Math.random() * 5) + 1; // dice 2 : 1 to 6 is randomly generated
        break;
      case "b": // if the user chooses to state the values 
      doB = 1; // if true (if the user has put down "b")
      System.out.println("Enter the first dice value: ");
      dice1 = myScanner.nextInt(); // accepts the input
      System.out.println("Enter the second dice value: ");
      dice2 = myScanner.nextInt(); // accepts the input
        break;
    }
    
    switch (doA) { // randomly generating
      case 1:
         switch (dice1)
        {
         case 1: // dice1 value = 1
            switch (dice2) // states the dice 2 values
          {
            case 1: 
             System.out.println("Snake Eyes");
             break;
            case 2:
             System.out.println("Ace Deuce");
             break;
            case 3:
             System.out.println("Easy Four");
             break;
            case 4:
             System.out.println("Fever Five");
             break;
            case 5:
             System.out.println("Easy Six");
             break;
            case 6:
             System.out.println("Seven Out");
             break;
          }
          break;
            case 2: // dice1 value = 2
             switch (dice2) // states the dice 2 values
            {
              case 2:
                System.out.println("Hard four");
              break;
              case 3:
                System.out.println("Fever five");
              break;
              case 4:
                System.out.println("Easy six");
              break;
              case 5:
                System.out.println("Seven out");
              break;
              case 6:
                System.out.println("Easy Eight");
              break;
            }
           break;
              case 3: // dice1 value = 3
                switch (dice2) // states the dice 2 values
               {
                  case 3:
                   System.out.println("Hard six");
                  break;
                  case 4:
                   System.out.println("Seven out");
                  break;
                  case 5:
                   System.out.println("Easy Eight");
                  break;
                  case 6:
                   System.out.println("Nine");
                  break;
                }
                break;
              case 4: // dice1 value = 4
                switch (dice2) // states the dice 2 values
              {
                 case 4:
                  System.out.println("Hard EIght");
                 break;
                 case 5:
                  System.out.println("Nine");
                 break;
                 case 6:
                  System.out.println("Easy Ten");
                 break;
              }
              break;
              case 5: // dice1 value = 5
               switch (dice2)
              {
                case 5:
                  System.out.println("Hard Ten");
                break;
                case 6:
                  System.out.println("Yo-Eleven");
                break;
               }
                break;
              case 6: // dice1 value = 6
                switch (dice2) 
              {
                 case 6:
                  System.out.println("Boxcars");
                break;
              }
              default: 
               break;
         }
                System.out.println("Dice 1: " + dice1 + ", Dice 2: " + dice2); // prints out the result for randomly generated data
  }
    
    switch (doB) { // user decided to state the values
      case 1:
         switch (dice1) 
        {
         case 1: // dice1 = 1
            switch (dice2) // states the dice 2 values
          {
            case 1:
             System.out.println("Snake Eyes");
             break;
            case 2:
             System.out.println("Ace Deuce");
             break;
            case 3:
             System.out.println("Easy Four");
             break;
            case 4:
             System.out.println("Fever Five");
             break;
            case 5:
             System.out.println("Easy Six");
             break;
            case 6:
             System.out.println("Seven Out");
             break;
          }
          break;
            case 2: // dice1 = 2
             switch (dice2) // states the dice 2 values
            {
              case 2:
                System.out.println("Hard four");
              break;
              case 3:
                System.out.println("Fever five");
              break;
              case 4:
                System.out.println("Easy six");
              break;
              case 5:
                System.out.println("Seven out");
              break;
              case 6:
                System.out.println("Easy Eight");
              break;
            }
           break;
              case 3: // dice1 = 3
                switch (dice2) // states the dice 2 values
               {
                  case 3:
                   System.out.println("Hard six");
                  break;
                  case 4:
                   System.out.println("Seven out");
                  break;
                  case 5:
                   System.out.println("Easy Eight");
                  break;
                  case 6:
                   System.out.println("Nine");
                  break;
                }
                break;
              case 4: // dice1 = 4
                switch (dice2) // states the dice 2 values
              {
                 case 4:
                  System.out.println("Hard EIght");
                 break;
                 case 5:
                  System.out.println("Nine");
                 break;
                 case 6:
                  System.out.println("Easy Ten");
                 break;
              }
              break;
              case 5: // dice1 = 5
               switch (dice2) // states the dice 2 values
              {
                case 5:
                  System.out.println("Hard Ten");
                break;
                case 6:
                  System.out.println("Yo-Eleven");
                break;
               }
                break;
              case 6: // dice1 = 6
                switch (dice2) // states the dice 2 values
              {
                 case 6:
                  System.out.println("Boxcars");
                break;
              }
             default: 
               break;
             }
         System.out.println("Dice 1: " + dice1 + ", Dice 2: " + dice2); // prints out the values that the user indicated and the corresponding slang term
        break;
    }
} // end of main method
} // end of class
   
 
        
    
    