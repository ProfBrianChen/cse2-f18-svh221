/// Suyeon Hong
/// 9/19/2018
/// CSE 002 - 210
/// lab04 Card Generator
/// To practice using if statements, switch statements, and Math.random(). 

import java.util.Scanner;
public class CrapsIf { // class starts
  public static void main(String arg[]) { // main method starts
    Scanner myScanner =  new Scanner ( System.in );
    System.out.println("If you want to randomly cast dice, type a. If you'd like to state the two dice, type b: ");
    
    String choice = "";
    
    choice = myScanner.nextLine();
    // System.out.println(choice);
    int dice1 = 7; // initializing
    int dice2 = 7; // initializing
    
    if (choice.equals("a")) { // if the user chooses to randomly cast the dice
      dice1 = (int)(Math.random() * 5) + 1; // randomly generates 1 -6
      dice2 = (int)(Math.random() * 5) + 1; 
      System.out.println("Dice 1: " + dice1 + ", Dice 2: " + dice2); // prints the input
    }
    else if (choice.equals("b")) { // if the user decides to state the values
      System.out.println("Enter the first dice value: "); // asks for the input
      dice1 = myScanner.nextInt(); // accepting the input
      System.out.println("Enter the second dice value: "); // asks for the input
      dice2 = myScanner.nextInt(); // accepting the input
    }
    else {
      System.out.println("That is not one of the options"); // if the user enters invalid alphabet
    }
    if ((dice1 == 1) && (dice2 == 1)) { // if dice 1 and dice 2 are both 1
       System.out.println("Snake Eyes"); // the slang term
    }
    if (((dice1 == 2) && (dice2 == 1)) || ((dice1 == 1 && dice2 == 2))) {
          System.out.println("Ace Deuce");
    }
    if (dice1 == 2 && dice2 == 2) {
          System.out.println("Hard four");
    }
    if ((dice1 == 3 && dice2 == 1) || (dice1 == 1 && dice2 == 3)) {
         System.out.println("Easy Four");
    }
    if ((dice1 == 3 && dice2 == 2) || (dice2 == 3 && dice1 == 2)) {
         System.out.println("Fever five");
       }
       if ((dice1 == 3 && dice2 == 3)) {
         System.out.println("Hard six");
        }
       if ((dice1 == 4 && dice2 == 1) || (dice2 == 4) && (dice1 == 1)) {
         System.out.println("Fever Five");
        }
      if ((dice1 == 4 && dice2 == 2) || (dice2 == 4 && dice1 == 2)) {
         System.out.println("Easy six");
      }
        if ((dice1 == 4 && dice2 == 3) || (dice2 == 4 && dice1 == 3)) {
         System.out.println("Seven out");
        }
          if (dice1 == 4 && dice2 == 4) {
         System.out.println("Hard Eight");
        }
      if ((dice1 == 5 && dice2 == 1) || (dice2 == 5 && dice1 == 1)) {
         System.out.println("Easy Six");
        }
      if ((dice1 == 5 && dice2 == 2) || (dice2 == 5 && dice1 == 2)) {
         System.out.println("Seven out");
        }
      if ((dice1 == 5 && dice2 == 3) || (dice2 == 5 && dice1 == 3)) {
         System.out.println("Easy Eight");
        }
      if ((dice1 == 5 && dice2 == 4) || (dice2 == 5 && dice1 == 4)) {
         System.out.println("Nine");
        }
      if (dice1 == 5 && dice2 == 5) {
         System.out.println("Hard Ten");
        }
      if ((dice1 == 6 && dice2 == 1) || (dice2 == 6 && dice1 == 1)) {
         System.out.println("Seven out");
        }
      if ((dice1 == 6 && dice2 == 2) || (dice2 == 6 && dice1 == 2)) {
         System.out.println("Easy Eight");
        }
      if ((dice1 == 6 && dice2 == 3) || (dice2 == 6 && dice1 == 3)) {
         System.out.println("Nine");
        }
      if ((dice1 == 6 && dice2 == 4) || (dice2 == 6 && dice1 == 4)) {
         System.out.println("Hard Ten");
        }
      if ((dice1 == 6 && dice2 == 5) || (dice2 == 6 && dice1 == 5)) {
         System.out.println("Yo-leven");
        }
      if (dice1 == 6 && dice2 == 6) {
         System.out.println("Boxcars");
        }
    if (dice1 > 6 || dice2 > 6) { // dice cannot have values greater than 6
      System.out.println("Incorrect Input"); 
  }
    } // end of main method
    } // end of class
     
    

    